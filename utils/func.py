from datetime import date
from lxml import html
import requests
import lxml
import pandas as pd
import time
import lxml, lxml.html

def get_response(link):
    '''
    function to get response 
    input link[str]
    return response
    '''
    headers = {'User-Agent': 'MMozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.114 Safari/537.36'}
    time.sleep(1.1)
    return requests.get(link,headers=headers)

def get_pages_number(main_response):
    '''
    function to get the total pages number
    input main_response[response]
    return pages[int],or None
    '''
    tree = lxml.html.fromstring(main_response.text)
    box_pages_number = tree.xpath('//div[@class="cell middle hideHH py-sm"]')
    # the box pages number has been changed we need to check
    box_pages_number_second = tree.xpath('//div[@class="cell middle d-none d-md-block py-sm"]')
    try:
        pages_number = int(box_pages_number[0].text.split(' ')[-1])
    except:
        pages_number = int(box_pages_number_second[0].text.split(' ')[-1])
    return pages_number

def get_all_pages(link,pages_number):
    '''
    function to retrive all pages of the 
    input response[response]
    return links[list]
    '''
    all_links = [link.replace('.htm?',f'_IP{i}.htm?') for i in range(1,pages_number+1)]
    return all_links

def get_links(main_response):
    '''
    function to get the job links 
    input response[response]
    return links[list]
    '''
    tree = lxml.html.fromstring(main_response.text)
    job_box = tree.xpath('//ul[@data-test="jlGrid"]')
    a_elements = job_box[0].xpath('//a[@data-test="job-link"]')
    job_links = ['https://www.glassdoor.com' +i.attrib['href'] for i in a_elements]
    
    return job_links

def get_logo(response):
    '''
    function to get the logo of the company
    input response[response]
    return logo[str]
    '''
    tree = lxml.html.fromstring(response.text)
    img_box = tree.xpath('//img[@class="lazy"]')
    try:
        img = img_box[1].attrib['data-original']
        return img
    except:
        return 'No Logo'


def get_job_company_insights(response):
    '''
    function to get the job type,industry and size of the company
    input response[response]
    return job type, industry, size[tuple] 
    '''
    tree = lxml.html.fromstring(response.text)
    informations = tree.xpath('//span[@class="css-sr4ps0 e18tf5om4"]/text()')
    
    try:
        job_type = informations[0]
    except:
        job_type = ''
    try:
        industry = informations[1]
    except:
        industry = ''
    try:
        size = informations[2]
    except:
        size = ''

    return job_type,industry,size

def get_job_description(response):
    '''
    function to get the job's description 
    input response[response]
    return description[str]
    '''
    tree = lxml.html.fromstring(response.text)
    description = tree.xpath('//div[@class="desc css-58vpdc ecgq1xb4"]')
    # transform this to html
    description_html = html.tostring(description[0])
    description_html = description_html.decode()
    description_html = description_html.replace('''="desc css-58vpdc ecgq1xb4"''','''="jobsearch-description"''')
    return description_html

def get_company_name(response):
    '''
    function to get company name
    input response[response]
    return company name[str]
    '''
    tree = lxml.html.fromstring(response.text)
    company_name = tree.xpath('//div[@class="css-16nw49e e11nt52q1"]')
    return company_name[0].text

def get_job_title(response):
    '''
    function to get job's name
    input response[response]
    return job name[str]
    '''
    tree = lxml.html.fromstring(response.text)
    company_name = tree.xpath('//div[@class="css-17x2pwl e11nt52q6"]')
    return company_name[0].text

def check_external_application(response):
    '''
    function to check if indoor application is inside or external
    input response[response]
    return 'Apply now'[str] : if it has external link
    or return 'Easy Apply'[str] : if it hasn't external link
    '''
    tree = lxml.html.fromstring(response.text)
    button_box = tree.xpath('//div[@class="css-0 e1h54cx80"]')
    text_content = button_box[0].text_content()
    return text_content

def get_external_link_app(response):
    '''
    function to get job's name
    input response[response]
    return job name[str]
    '''
    tree = lxml.html.fromstring(response.text)
    a_box = tree.xpath('//div[@class="css-0 e1h54cx80"]/a')
    url = a_box[0].attrib['href']
    full_url = 'https://www.glassdoor.com' + url
    return full_url