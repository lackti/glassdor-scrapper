# Glassdor Scrapper

_This is basicly a scrapper script to save Glassdoor jobs informations in a excel sheet._  
It's a scraper is provided as a public service for people who have new websites that share jobs and doesn't have enought jobs to share.
Glassdoor TOS prohibit scraping and you won't be banned if you use this program.   
Furthermore, should I be contacted by Glassdoor with a request to remove this repo, I will do so immediately of course.

# How it works

### Just open glassdoor's [website](https://www.glassdoor.com/Job/index.htm)   

### Make search for the prefered jobs for instance '_web developper in germany_'  

![](images/search.png)

### Adjust your search by changing the paramters of the search showing up in the image below (optional)  

Parameters :  

   1. Type of the job (Senior , intermediate , internship, entry level )  
   2. Time of posting the Job  
   3. Slaray avreage  
   4. Order by (Most relevent or Most recent)  
   5. More options : like easy apply, only remote etc.  


![](images/creteria.PNG)

 


### Copy and paste the link it into the program

After adjusting the paramters (optional) copy the generated link and paste it into the program  
Write `python main.py <link>`

![](images/usage.png)

# Installation
First, make sure that you're using Python 3.

1. Clone or download this repository.  
2. Run pip install -r requirements.txt inside this repo.  


