import os
import sys
from pandas import ExcelWriter
from utils.func import *

link = sys.argv[1]

request = get_response(link)
number_of_pages = get_pages_number(request)

print(f"There's {number_of_pages} pages to be scrapped.")#inchallah

all_pages = get_all_pages(link,number_of_pages)

jobs_links = []
for number,page in enumerate(all_pages):
    '''
    loop throught the main page and retrive the job links
    '''
    page_nbr = number+1
    try:
        response = get_response(page)
        jobs_links.extend(get_links(response))
        print(f'getting the job links for the page number {page_nbr}')
    except:
        print(f'an error has been occured in page number {page_nbr}')
    

orginal_df = pd.DataFrame(columns=['Company','Job Title','Job Type',
                                    'Industry','Size','Job Description'
                                   ,'Glassdoor app','External App','Url Logo'])
jobs_found = len(jobs_links)
print(f'{jobs_found} jobs has been found')

i = 1
for job_link in jobs_links[:5]:
    row = []
    try:
        response = get_response(job_link)
        company = get_company_name(response)
        row.append(company)
        job_title = get_job_title(response)
        row.append(job_title)

        job_type, industry, size = get_job_company_insights(response)
        row.append(job_type)
        row.append(industry)
        row.append(size)

        description = get_job_description(response)
        row.append(description)
        
        glassdoor_app = job_link
        row.append(glassdoor_app)

        external_or_not = check_external_application(response)

        if 'Easy Apply' in external_or_not :
            external_application = 'Indoor Application'
            row.append(external_application)
        else:
            redirected_link = get_external_link_app(response)
            original_link_response = get_response(redirected_link)
            external_application = original_link_response.url
            row.append(external_application)
        
        logo = get_logo(response)
        row.append(logo)

        orginal_df.loc[i] = row
        print(f'{job_title} : has been added to the sheet')
            
    except:
        print('an error has been occured')
    finally:
        i += 1

today = date.today()
print(today)
writer = ExcelWriter(f'glassdoor {today}.xlsx')
orginal_df.to_excel(writer,'Sheet5',index=False)
writer.save()
print(f'Sheet glassdoor {today}.xlsx is successfully created')